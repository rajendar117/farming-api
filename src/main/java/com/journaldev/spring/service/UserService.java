package com.journaldev.spring.service;

import java.sql.SQLException;

import com.journaldev.spring.model.Users;

public interface UserService {
	public boolean isValidUser(String username,String password) throws SQLException ;
	public void userRegistration(Users u);
	public void updateUser(Users u);
}
