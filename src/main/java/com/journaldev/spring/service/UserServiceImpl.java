package com.journaldev.spring.service;

import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.journaldev.spring.dao.UserDAO;
import com.journaldev.spring.model.Users;
@Service
public class UserServiceImpl implements UserService{
	
	private UserDAO userDAO;
	
	public UserDAO getUserDAO() {
		return userDAO;
	}



	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}



	@Override
	@Transactional
	public boolean isValidUser(String username, String password)throws SQLException  {
		// TODO Auto-generated method stub
		return userDAO.isValidUser( username,  password);
	}



	@Override
	public void userRegistration(Users u) {
		// TODO Auto-generated method stub
		this.userDAO.userRegistration(u);
	}



	@Override
	public void updateUser(Users u) {
		// TODO Auto-generated method stub
		this.userDAO.updateUser(u);
	}
	
	

}
