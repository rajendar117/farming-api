package com.journaldev.spring;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.journaldev.spring.model.Person;
import com.journaldev.spring.model.Users;
import com.journaldev.spring.service.UserService;

@Controller
@RequestMapping("/userService")
public class UserController {	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	private UserService  userService;

	@Autowired(required=true)
	@Qualifier(value="userService")
	public void setPersonService(UserService  us){
		this.userService = us;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/login", method = RequestMethod.POST, headers="Accept=application/json")
	public @ResponseBody JSONObject login(HttpServletRequest request, @ModelAttribute("user") Users users )throws SQLException {
		JSONObject userData = null;
		try{
			boolean isValidUser = userService.isValidUser(users.getEmail(), users.getPassword()) ;
			if (isValidUser) {
				//System.out.println("User Login Successful");
				logger.info("User Login Successful");

			} else {
				//System.out.println("User Login Not Successful");
				logger.info("User Login Not Successful");
			}
		}catch(Exception e){
			logger.info("Exception in UserService : ="+e);
		}

		return userData;
	}
	
	
	//For add and update person both
		@RequestMapping(value= "/person/add", method = RequestMethod.POST, headers="Accept=application/json")
		public JSONObject registration(@ModelAttribute("users") Users user){
			JSONObject userData = null;
			if(user.getId() == 0){
				//new person, add it
				this.userService.userRegistration(user);
			}else{
				//existing person, call update
				this.userService.updateUser(user);
			}
			
			return userData;
			
		}


}