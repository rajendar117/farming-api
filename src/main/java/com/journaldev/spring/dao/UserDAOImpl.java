package com.journaldev.spring.dao;

import java.sql.SQLException;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.journaldev.spring.model.Users;

@Repository
public class UserDAOImpl implements UserDAO {

	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	@Override
	public boolean isValidUser(String username, String password) throws SQLException {
		// TODO Auto-generated method stub
		boolean validFlag=false;
		Session session = this.sessionFactory.getCurrentSession();
		//Query query=session.createQuery("SELECT e FROM Users e WHERE e.email = ? and  e.password = ?");
		Query query=session.getNamedQuery("Users.findByemailandPassword");
		query.setString("email", username);
		query.setString("password", password);
		Users user =  (Users) query.uniqueResult();
		if(user!=null){
			validFlag = true;
		}
		System.out.println("User is :"+user.getEmail()+"Password is :"+user.getPassword());
		return validFlag;


	}
	@Override
	public void userRegistration(Users u) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(u);
		logger.info("Person saved successfully, Person Details="+u);
	}
	@Override
	public void updateUser(Users u) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(u);
		logger.info("Person updated successfully, Person Details="+u);
	}
	
}
