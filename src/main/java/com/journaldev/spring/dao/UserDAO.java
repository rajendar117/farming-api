package com.journaldev.spring.dao;

import java.sql.SQLException;

import com.journaldev.spring.model.Users;

public interface UserDAO {
	public boolean isValidUser(String username,String password)throws SQLException ;
	public void userRegistration(Users u);
	public void updateUser(Users u);
}
